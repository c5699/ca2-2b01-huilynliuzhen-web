from flask_wtf import FlaskForm
from wtforms import FloatField, SubmitField, RadioField, StringField, PasswordField
from wtforms.validators import Length, InputRequired, ValidationError, NumberRange

class LoginForm(FlaskForm):
    name   = StringField("Name", validators=[InputRequired()])
    password   = PasswordField("Password", validators=[InputRequired(), Length(5,20)])
    submit = SubmitField("Login")