from application import db
import datetime as dt
from flask_login import UserMixin
from sqlalchemy.orm import validates 

class User(db.Model, UserMixin):
    __tablename__ = 'user'
 
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(20))
    password = db.Column(db.String(20))
 
# class Entry(db.Model):
#     __tablename__ = 'predictions'
 
#     id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     user_id = db.Column(db.Integer, db.ForeignKey(User.id)) 
#     imgfilepath = db.Column(db.String(32))
#     results1 = db.Column(db.String(32))
#     results2 = db.Column(db.String(32))
#     results3 = db.Column(db.String(32))
#     predicted_on = db.Column(db.DateTime, nullable=False)
    
class Entry(db.Model):
    __tablename__ = 'predictions'
 
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id)) 
    imgfilepath = db.Column(db.String(32))
    results1 = db.Column(db.String(32))
    results1Percent = db.Column(db.Float)
    results2 = db.Column(db.String(32))
    results2Percent = db.Column(db.Float)
    results3 = db.Column(db.String(32))
    results3Percent = db.Column(db.Float)
    predicted_on = db.Column(db.DateTime, nullable=False)


    @validates('results1Percent') 
    def validate_results1Percent(self, key, results1Percent):
        if results1Percent <=0:
            raise AssertionError('Value must be positive')
        elif results1Percent > 100:
            raise AssertionError("Percentage can't be more than 100")
        
        return results1Percent

    @validates('results2Percent') 
    def validate_crossLen(self, key, results2Percent):
        if results2Percent <=0:
            raise AssertionError('Value must be positive')
        elif results2Percent > 100:
            raise AssertionError("Percentage can't be more than 100")
        return results2Percent

    @validates('results3Percent') 
    def validate_crossLen(self, key, results3Percent):
        if results3Percent <=0:
            raise AssertionError('Value must be positive')
        elif results3Percent > 100:
            raise AssertionError("Percentage can't be more than 100")
        return results3Percent