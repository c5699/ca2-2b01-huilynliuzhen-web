from application import app, db
from application.models import Entry, User
from flask_login import login_user, logout_user, login_required, LoginManager, current_user
from flask.globals import session
from datetime import datetime
from flask import json, jsonify, redirect, abort
from flask import render_template, request, flash
from flask_cors import CORS, cross_origin
from keras.preprocessing import image
from PIL import Image, ImageOps
import numpy as np
import keras.models
import os
from sqlalchemy import select, desc
import re
import base64
import pandas as pd
from io import BytesIO
# from tensorflow.keras.datasets.mnist import load_data
import json
import numpy as np
import requests
import pathlib, os
from application.forms import LoginForm

#create the database if not exist
db.create_all()

# Login stuffs
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = '/login'
@login_manager.user_loader #activates after login_post()

#FRONT END==============================================
def load_user(user_id):
    return User.query.get(int(user_id))
def remove_entry(id):
    try:
        entry = Entry.query.get(id)
        # Delete file from folder:
        os.remove(f'./application/static/uploadedImg/{entry.imgfilepath}')

        #Deletes entry from DB
        db.session.delete(entry)
        db.session.commit()
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

def add_entry(new_entry):
    try:
        db.session.add(new_entry)
        db.session.commit()
        return new_entry.id
 
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

def get_entries(userid):
    try:
        entries = Entry.query.filter_by(user_id=userid)
        return entries
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0

def get_latest(userid):
    try:
        latest = Entry.query.filter_by(user_id=userid).order_by(desc('predicted_on')).first()
        return latest
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0


# Handles history page
@app.route('/history', methods=['GET']) 
@login_required
def history_page(): 
    userid = session.get("_user_id")
    print("USER ID IS ", userid)
    return render_template('history.html',title="History",latest = get_latest(userid),entries = get_entries(userid))
    
def get_entriesAll():
    try:
        entries = Entry.query.all()
        return entries
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0

def parseImage(imgData):
    # parse canvas bytes and save as output.png
    userid = session.get("_user_id")
    now = datetime.now()
    date = str(now.year)+"_"+str(now.month)+"_"+str(now.day)
    time = str(now.hour)+str(now.minute)+str(now.second)
    saveStr = str(userid)+"_"+str(date)+"_"+str(time)+".png"
    imgstr = re.search(b'base64,(.*)', imgData).group(1)

    with open('test100.png','wb') as output:
        output.write(base64.decodebytes(imgstr))
    im = Image.open('test100.png').convert('RGB')
    # im_invert = ImageOps.invert(im)
    saveStr2 = f'./application/static/uploadedImg/{saveStr}'
    im.save(saveStr2)
    return saveStr
 
def make_prediction(instances):
    data = json.dumps({"signature_name": "serving_default", "instances": instances.tolist()})
    headers = {"content-type": "application/json"}
    json_response = requests.post(url, data=data, headers=headers)
    # print("json response is ",json_response)
    predictions = json.loads(json_response.text)['predictions']
    return predictions

def add_user(new_user):
    try:
        db.session.add(new_user)
        db.session.commit()
        return new_user.id

    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

#server URL
url = 'https://plantserver420.herokuapp.com/v1/models/img_classifier:predict'



#LOGIN PAGE=========
@app.route('/', methods=['GET', 'POST']) 
@app.route('/index', methods=['GET', 'POST']) 
@app.route('/login', methods=['GET','POST'])
def login_post():
    # Check if there is a default account inside the database
    # default = "Sharon"
    # default_pass = "Ilovefish28"
    default = "JohnFoo"
    default_pass = "Higuys123"
    
    root_check = User.query.filter_by(username = default).first()
    if root_check is None:
        # If there is no default account, create one
        default_user = User(username = default,password = default_pass)
        add_user(default_user)
    else:
        pass
    form = LoginForm()
    if request.method == 'GET':
        return render_template('login.html', title="Login", form=form, index=True)

    if request.method == 'POST':  
        if form.validate_on_submit():      
            name = form.name.data
            password = form.password.data

            user = User.query.filter_by(username=name).first()
            print(user)
            if user == None:
                flash('Wrong username/password!',"danger")
                return redirect("/login")
            elif name == user.username and password == user.password:
                global current_user
                global USERID
                USERID = user.id
                print("THE USER ID IS ", USERID)
                current_user = user.username
                login_user(user)
                session['username'] = current_user
                return redirect("/home")
            else:
                flash('Wrong username/password!',"danger")
                return redirect("/login")
        else: 
            flash('Wrong username/password!',"danger")
    return redirect("/login")

#Handles http://127.0.0.1:5000/
@app.route('/home',methods=['GET','POST'])
@login_required
def index_page(): 
    # print("IN HOME, USERID IS ", current_user.id)
    # print(request.method)
    if request.method == 'GET':
        return render_template('index.html', title="Plant Doctor", index=True)

    # return redirect("/home")

# Remove a history record========================
@app.route('/remove', methods=['POST'])
def remove():
    req = request.form
    id = req["id"]
    remove_entry(id)
    userid = session.get("_user_id")
    return render_template("history.html", 
        title="History", 
        entries = get_entries(userid), latest = get_latest(userid),index=True )

#LOGOUT
@app.route('/logout') 
@login_required
def logout():
    flash("Logged out successfully!","success")
    logout_user()
    session.clear()
    print(current_user)
    print(session)
    return redirect("/login") 


# #Handles http://127.0.0.1:5000/predict
@app.route("/predict", methods=['POST'])
@login_required
@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
def predict():
    print("in PREDICT")
    filePath = parseImage(request.get_data())
    filePath2 = f'./application/static/uploadedImg/{filePath}'
    # Decoding and pre-processing base64 image
    img_height = 180
    img_width = 180
    img = image.load_img(filePath2, target_size=(img_height, img_width))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
 
    predictions = make_prediction(x)
    print("PREDICTIONS IS ", predictions[0])
    predictions2 = predictions[0]
    ret = ""
    class_names=  ['complex', 'frog_eye_leaf_spot', 'healthy', 'multiple_diseases', 'powdery_mildew', 'rust', 'scab']
    # PREDICTIONS IS  [2.5482953e-07, 1.82049334e-07, 0.00068973907, 2.03388627e-05, 0.980126739, 8.61432054e-05, 0.0190766435]
    print("PREDICTIOSN sort",np.sort(predictions2[::-1]))
    predSort = sorted(predictions2,reverse=True)
    predPercent = []
    for i in range(0,3):
        onePredPercent = predSort[i] * 100
        predPercent.append(onePredPercent)
    
    print("PRED PERCENT IS ", predPercent)
    d = {
        class_names[0]: predictions2[0],
        class_names[1]: predictions2[1],
        class_names[2]: predictions2[2],
        class_names[3]: predictions2[3],
        class_names[4]: predictions2[4],
        class_names[5]: predictions2[5],
        class_names[6]: predictions2[6],
    }
    my_keys = sorted(d, key=d.get, reverse=True)[:3]
    
    userid =session.get("_user_id")
    entry = Entry(user_id= userid, imgfilepath=filePath, results1=my_keys[0],
        results1Percent=predPercent[0],results2=my_keys[1],results2Percent=predPercent[1],
        results3=my_keys[2],results3Percent=predPercent[2],
        predicted_on=datetime.utcnow())
    add_entry(entry)   
    # Returns "success" to ajax
    return jsonify({'result':'ok'})

# If page does not exist (404 PAGE)
@app.errorhandler(404)
def resource_not_found(e):
    return render_template("404.html")


# Handling standard errors
@app.errorhandler(Exception)
def server_error(err):
    app.logger.exception(err)
    return "exception", 500


# API TESTING ============================================================


#API: add entry
@app.route("/api/add", methods=['POST'])
def api_add(): 
    #retrieve the json file posted from client
    data = request.get_json()
    #retrieve each field from the data
    imgfilepath     = data['imgfilepath']
    results1     = data['results1']
    results1Percent     = data['results1Percent']
    results2     = data['results2']
    results2Percent  = data['results2Percent']
    results3     = data['results3']
    results3Percent  = data['results3Percent']
    #create an Entry object store all data for db action
    new_entry = Entry(  imgfilepath=imgfilepath, results1=results1,
                        results1Percent=results1Percent,results2=results2,
                        results2Percent = results2Percent, results3=results3,
                        results3Percent=results3Percent,
                        predicted_on=datetime.utcnow())
    #invoke the add entry function to add entry                        
    result = add_entry(new_entry)
    #return the result of the db action
    return jsonify({'id':result})

def get_entry(id):
    try:
        entries = Entry.query.filter(Entry.id==id)
        result = entries[0]
        return result
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0 

#API get entry
@app.route("/api/get/<id>", methods=['GET'])
def api_get(id): 
    #retrieve the entry using id from client
    entry = get_entry(int(id))
    print("ENTRY IS : ",entry)
    #Prepare a dictionary for json conversion
    data = {'id'        : entry.id,
            'imgfilepath'   : entry.imgfilepath, 
            'results1'   : entry.results1,
            'results1Percent'   : entry.results1Percent,
            'results2'   : entry.results2,
            'results2Percent': entry.results2Percent,
            'results3'   : entry.results3,
            'results3Percent': entry.results3Percent}
    #Convert the data to json
    result = jsonify(data)
    return result #response back

# GET ALL ENTRIES 
@app.route("/api/get", methods=['GET'])
def api_getAll(): 
    def add_element(dict, key, value):
        if key not in dict:
            dict[key] = []
        dict[key].append(value)

    test = {}
    entry = get_entriesAll()
    print("ENTRIES IS ", entry)
    # print("ENTRIES: ", entry[0].id, entry[1].id)
    # Prepare a dictionary for json conversion
    for i in range(0,len(entry)):
        add_element(test, 'id',entry[i].id)
        add_element(test, 'imgfilepath',entry[i].imgfilepath)
        add_element(test,'results1',entry[i].results1)
        add_element(test,'results1Percent',entry[i].results1Percent)
        add_element(test,'results2',entry[i].results2 )
        add_element(test,'results2Percent',entry[i].results2Percent)
        add_element(test,'results3',entry[i].results3 )
        add_element(test,'results3Percent',entry[i].results3Percent)
    result = jsonify(test)
    return result

# #Handles http://127.0.0.1:5000/predict
@app.route("/api/predict", methods=['POST'])
def api_predict():
    #retrieve the json file posted from client
    data = request.get_json()
    #retrieve each field from the data
    filePath = data['imgfilepath']
    filePath2 = f'./application/static/uploadedImg/{filePath}'
    # Decoding and pre-processing base64 image
    img_height = 180
    img_width = 180
    img = image.load_img(filePath2, target_size=(img_height, img_width))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    predictions = make_prediction(x)
    predictions2 = predictions[0]
    ret = ""
    class_names=  ['complex', 'frog_eye_leaf_spot', 'healthy', 'multiple_diseases', 'powdery_mildew', 'rust', 'scab']
    # PREDICTIONS IS  [2.5482953e-07, 1.82049334e-07, 0.00068973907, 2.03388627e-05, 0.980126739, 8.61432054e-05, 0.0190766435]
    print("PREDICTIOSN sort",np.sort(predictions2[::-1]))
    predSort = sorted(predictions2,reverse=True)
    predPercent = []
    for i in range(0,3):
        onePredPercent = predSort[i] * 100
        predPercent.append(onePredPercent)
    
    print("PRED PERCENT IS ", predPercent)
    d = {
        class_names[0]: predictions2[0],
        class_names[1]: predictions2[1],
        class_names[2]: predictions2[2],
        class_names[3]: predictions2[3],
        class_names[4]: predictions2[4],
        class_names[5]: predictions2[5],
        class_names[6]: predictions2[6],
    }
    my_keys = sorted(d, key=d.get, reverse=True)[:3]
    
    data = {'Results 1 ': my_keys[0], 'Results 1 %': predPercent[0], 'Results 2': my_keys[1],
            'Results 2 %': predPercent[1], 'Results 3': my_keys[2], 'Results 3 %': predPercent[2]}
    # Returns "success" to ajax
    return jsonify(data)

def api_remove_entry(id):
    try:
        entry = Entry.query.get(id)
        db.session.delete(entry)
        db.session.commit()
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")
#API delete entry
@app.route("/api/delete/<id>", methods=['GET'])
def api_delete(id): 
    entry = api_remove_entry(int(id))
    return jsonify({'result':'ok'})