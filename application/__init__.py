from flask import Flask
from flask_cors import CORS

#For persistent storage
from flask_sqlalchemy import SQLAlchemy

#create the Flask app
app = Flask(__name__)
CORS(app)
 
# load configuration from config.cfg
app.config.from_pyfile('config.cfg')
# instantiate SQLAlchemy to handle db process
db = SQLAlchemy(app)

from application import routes
