from application.models import Entry, User
import datetime as datetime
import pytest
from flask import json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
from time import sleep
import sys

#Unit Test
#imgfilepath, results1, results1Percent, results2, results2Percent, results3, results3Percent
@pytest.mark.parametrize("entrylist",[
    [  'test200.jpg' ,  'complex',  96.30000,'scab', 2.2222,'rust', 1.2934],  #Test integer arguments [PASS]
    [   -10,  -10,  -1, -350,-10,-10,10],  #Expected to fail
    ['test200.jpg','healthy', 94, 'rust', 2, 'scab', 1],   #Test Int arguments [PASS]
    ['b','c','d','e','f','g','h'], #Test string 
    [-10,4,'d',None,None,1,1], #test weird vals
    [ 'test200.jpg' ,  'complex',  1500.002,'scab', 500.2000,'rust', -1000.2], #Test out of range values
])
@pytest.mark.xfail(reason="string fields not string or float fields not float")

def test_EntryClass(entrylist,capsys):
    with capsys.disabled():
        print(entrylist)
        now = datetime.datetime.utcnow()
        new_entry = Entry(  
                            
                            imgfilepath = entrylist[0],
                            results1= entrylist[1],
                            results1Percent = entrylist[2],
                            results2  = entrylist[3],  
                            results2Percent= entrylist[4],
                            results3 = entrylist[5],
                            results3Percent = entrylist[6],
                            predicted_on= now) 
 
        assert new_entry.imgfilepath   == entrylist[0]
        assert new_entry.results1    == entrylist[1]
        assert new_entry.results1Percent   == entrylist[2]
        assert new_entry.results2    == entrylist[3]
        assert new_entry.results2Percent     == entrylist[4]
        assert new_entry.results3    == entrylist[5]
        assert new_entry.results3Percent   == entrylist[6]
        assert new_entry.predicted_on   == now
@pytest.mark.xfail(reason="arguments <= 0")

def test_EntryValidation(entrylist,capsys):
    test_EntryClass(entrylist,capsys)


#Test add API
@pytest.mark.parametrize("entrylist",[
    ['test200.jpg','complex',96.30000,'scab', 2.2222,'rust', 1.2934],  #Test integer arguments Expected to pass
    ['test200.jpg','healthy', 94, 'rust', 2, 'scab', 1]])
def test_addAPI(client,entrylist,capsys):
    with capsys.disabled():
        #prepare the data into a dictionary
        data1 = {   'imgfilepath': entrylist[0], 
                    'results1' : entrylist[1],
                    'results1Percent': entrylist[2],
                    'results2' : entrylist[3],
                    'results2Percent': entrylist[4],
                    'results3' : entrylist[5],
                    'results3Percent' : entrylist[6]}
        #use client object  to post
        #data is converted to json
        #posting content is specified
        response = client.post('/api/add', 
            data=json.dumps(data1),
            content_type="application/json")
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["id"]
# @pytest.mark.xfail(reason="arguments not in range")

#Test get API
@pytest.mark.parametrize("entrylist",[
    ['test200.jpg','complex',96.30000,'scab', 2.2222,'rust', 1.2934,1],  #Test integer arguments Expected to pass
    ['test200.jpg','healthy', 94, 'rust', 2, 'scab', 1,2]
    #['c','b',-2,10, 'a', 3] #this one can't be inserted, so will fail.
])

def test_getAPI(client,entrylist,capsys):
    with capsys.disabled():
        response = client.get(f'/api/get/{entrylist[7]}')
        ret = json.loads(response.get_data(as_text=True))
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["id"] == entrylist[7]
        assert response_body["imgfilepath"] == str(entrylist[0])
        assert response_body["results1"] == str(entrylist[1])
        assert response_body["results1Percent"] == float(entrylist[2])
        assert response_body["results2"] == str(entrylist[3])
        assert response_body["results2Percent"] == float(entrylist[4])
        assert response_body["results3"] == str(entrylist[5])
        assert response_body["results3Percent"] == float(entrylist[6])

#Test get all API
def test_getAllAPI(client,capsys):
    with capsys.disabled():
        entrylist =   [  ['test200.jpg','complex',96.30000,'scab', 2.2222,'rust', 1.2934,1],
        ['test200.jpg','healthy', 94, 'rust', 2, 'scab', 1,2]]
        response = client.get(f'/api/get')
        
        ret = json.loads(response.get_data(as_text=True))
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        for i in range(0, len(response_body['id'])):
            assert response_body["id"][i] == entrylist[i][7]
            assert response_body["imgfilepath"][i] == str(entrylist[i][0])
            assert response_body["results1"][i] == str(entrylist[i][1])
            assert response_body["results1Percent"][i] == float(entrylist[i][2])
            assert response_body["results2"][i] == str(entrylist[i][3])
            assert response_body["results2Percent"][i] == float(entrylist[i][4])
            assert response_body["results3"][i] == str(entrylist[i][5])
            assert response_body["results3Percent"][i] == float(entrylist[i][6])

#Predict API
@pytest.mark.parametrize("entrylist", [['test.jpg']])
def test_predict_API(client, entrylist, capsys):
    with capsys.disabled():
        #prepare data in to a dictionary
        data1 = {
            'imgfilepath' : entrylist[0],
        }
        #use client object to post
        #data is converted to json
        #posting content is specified
        response = client.post('/api/predict',
            data=json.dumps(data1),
            content_type="application/json")

        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers['Content-Type'] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        print(response_body)

#Delete API
#Test delete API
@pytest.mark.parametrize("ind",[1,2])
def test_deleteAPI(client,ind,capsys):
    with capsys.disabled():
        response = client.get(f'/api/delete/{ind}')
        ret = json.loads(response.get_data(as_text=True))
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["result"] == "ok"
