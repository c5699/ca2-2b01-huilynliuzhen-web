from lib2to3.pgen2 import driver
import pytest
from app import app as flask_app
 
@pytest.fixture
def app():
    yield flask_app
 
@pytest.fixture
def client(app):
    print(app.static_url_path)
    return app.test_client()

# from selenium import webdriver

# browsers = {
#     "firefox": webdriver.Firefox,
#     "chrome": webdriver.Chrome,
# }

# @pytest.fixture(params = browsers.keys())
# def browser(request):
#     browser_name = request.param
#     driver = browsers.get(browser_name)()
#     yield driver
#     driver.quit()

# def test_demo(browser):
#     browser.get("https://www.google.com/")